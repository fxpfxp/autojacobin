clear;
clc;
close all;

%load the whole training dataset of wavelet feature in NUS-WIDE
load(['dataset/NUSWIDEWT_Learn_features.mat']);  
X =Learn_features;
X=double(X);

%training with NOKMeans
AutoJacoBinparam.nbits=64;
AutoJacoBinparam.normalizescale=0.8;
AutoJacoBinparam.beta=0.1;
AutoJacoBinparam = trainAutoJacoBin(X, AutoJacoBinparam);

% extrac the base dataset
load(['dataset/NUSWIDEWT_Based_features.mat']);  
base =Base_features;
% encode the base dataset
Btraining = encodeAutoJacoBin(base, AutoJacoBinparam)';

% extrac the query dataset
load(['dataset/NUSWIDEWT_Query_features.mat']);  
query =Query_features;
% encode the query dataset
Btest = encodeAutoJacoBin(query, AutoJacoBinparam)';

%extract the ground truth
load(['dataset/NUSWIDEWT_Query_features.mat']);  
gt =gnd; 

%random select `nquery', and the calculate the Recall@i performance
nquery=1000;
Kh=10000;
Ke=10;
indeq=randperm(size(query,2),nquery);
Recalli=[];
for i=1:nquery
    Dhi = hammingDist(Btest(:,indeq(i))', Btraining');
    [~,index] = sort(Dhi);
    indexH = index(:,1:Kh);
    indexE = gt(indeq(i), 1:Ke);
   
    Rtrue = (distMat(indexE', indexH')== 0);
    Recalli = [Recalli; cumsum(sum(Rtrue, 1))/Ke];  
end

Recalli=mean(Recalli,1);