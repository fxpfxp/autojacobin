function [B, U,UO] = encodeAutoJacoBin(X, AutoJacoBinparam)
W=AutoJacoBinparam.W;
b1=AutoJacoBinparam.b1;
maxnorm=AutoJacoBinparam.maxnorm;
UO=(W*((X)/(maxnorm))+repmat(b1,1,size(X,2)))';
TrainingBinary=sign(W*((X)/(maxnorm))+repmat(b1,1,size(X,2)))';
B = compactbit(TrainingBinary>0);
U = (UO>0);
