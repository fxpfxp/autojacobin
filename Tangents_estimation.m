function T = Tangents_estimation(X,hiddenSize1,nTangent)
[Dim,n]=size(X)
['calculate the Distance....']
Dist = ones(n, 1) * sum((X(:,1:nTangent)).^2) + sum(X.^2)'*ones(1,nTangent) - 2*(X'*X(:,1:nTangent));
['sort the distances ...']
[J,I]=sort(Dist);
T=zeros(Dim,Dim,nTangent);
['calculate the tangent spaces ...']
K= min([Dim*2,Dim+hiddenSize1]);
for i=1:nTangent
    x=X(:,I(1:K,i))-repmat(mean(X(:,I(1:K,i)),2),[1,K]);
    [V,D]=eig(cov(x'));
    [ID indexD]=sort(diag(D),'descend');
    energy=cumsum(diag(ID))/sum(diag(ID));
    maxindes=min([find(energy>0.98); hiddenSize1]);
    T(:,:,i)=V(:,indexD(1:maxindes))*V(:,indexD(1:maxindes))';
end

end