function theta = initializeParameters_seltcontr_pca(X,visibleSize, hiddenSize1,bits)
mu = mean(X, 2);
X_mu = bsxfun(@minus, X, mu);
[pc, l] = eigs(cov(double(X_mu')),bits);
R = randn(bits,bits);
[U11 S2 V2] = svd(R);
pc=pc*U11;

W1 = pc';
W2 = pc;
b1 = -W1*mu;
b2 = mu;
% flatten the paramters into a vector
theta = [W1(:) ;W2(:) ; b1(:) ; b2(:)];
end