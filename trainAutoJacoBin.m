function AutoJacoBinparam = trainAutoJacoBin(X, AutoJacoBinparam)

maxnorm=sqrt(max(sum(X.^2)))/AutoJacoBinparam.normalizescale;
X=X/maxnorm;
AutoJacoBinparam.maxnorm=maxnorm;

bit=AutoJacoBinparam.nbits; 
visibleSize = size(X,1);   % number of input units 
hiddenSize = bit;     % number of hidden units
nTangent=size(X,2);
T = Tangents_estimation(X,bit,nTangent);

theta = initializeParameters_seltcontr_pca(X,visibleSize,hiddenSize,bit);
aotu=1;
contr=1;
beta=AutoJacoBinparam.beta;
opttheta=Hashing_AutoencoderCost_LineaSearch(theta,visibleSize,hiddenSize, aotu,contr,beta,T, double(X));

AutoJacoBinparam.W = reshape(opttheta(1:hiddenSize*visibleSize), hiddenSize, visibleSize);
opttheta(1:hiddenSize*visibleSize)=[];
W2 = reshape(opttheta(1:hiddenSize*visibleSize), visibleSize, hiddenSize);
opttheta(1:hiddenSize*visibleSize)=[];
AutoJacoBinparam.b1 = opttheta(1:hiddenSize);
opttheta(1:hiddenSize)=[];
b2 = opttheta(1:visibleSize);
opttheta(1:visibleSize)=[];