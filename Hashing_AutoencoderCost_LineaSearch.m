function [grad] = Hashing_AutoencoderCost_LineaSearch(theta, visibleSize,hiddenSize1,auto, contr, beta,Ts, data)
batchite=10;
batchsize=size(data,2)/batchite;
for ite=1:5
    shuffle=randperm(size(data,2));
    for batch=1:batchite  
        databatch=data(:,shuffle(batchsize*(batch-1)+1:batchsize*batch));
        Tsbatch=Ts(:,:,shuffle(batchsize*(batch-1)+1:batchsize*batch));
                
        %  Use minFunc to minimize the function
        addpath minFunc_2012/minFunc

        %options.Method = 'lbfgs'; 
        options.Method = 'sd';  
        options.maxIter = 1;	  
        options.display = 'on';

        [theta, cost] = minFunc( @(p) Hashing_AutoencoderCost_selfcontr_robust_lite(p, ...
                                            visibleSize,hiddenSize1, auto,contr,beta,Tsbatch, databatch), ...
                                      theta, options);
        costline=cost
    end
end
grad=theta;
end